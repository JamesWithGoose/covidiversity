<?php
/***********************************
* Author: James Holden
* Created: 3/25/2020
* Covidiversity is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
***********************************/


session_start();
header("Cache-control: private"); //IE 6 Fix
include_once('startup.php');   

$val_ses = guid();
$_SESSION['val_ses'][$val_ses] = 1;


$def_val = array('count' => 25,
                 'max1' => '',
                 'max10' => '',
                 'max100' => 'checked',
                 'max1000' => '',
                 'addition' => 'checked',
                 'subtraction' => 'checked',
                 'multiplication' => 'checked',
                 'division' => 'checked',
                 'none' => 'checked',
                 'qst' => '',
                 'useX' => '',
                 );


if($_POST['secCntrl'] <> '' and $_SESSION['val_ses'][$_POST['secCntrl'] ] == 1){
    
    $def_val = array('count' => 0,
                     'max1' => '',
                     'max10' => '',
                     'max100' => '',
                     'max1000' => '',
                     'addition' => '',
                     'subtraction' => '',
                     'multiplication' => '',
                     'division' => '',
                     'none' => '',
                     'qst' => '',
                     'useX' => '',
                     );
        
    
    $prob_pdf = new FPDF('P','mm','letter');
    $prob_pdf->SetMargins(8,8,8);
    $prob_pdf->SetAutoPageBreak(true,12);
    $ans_pdf = new FPDF('P','mm','letter');
    $ans_pdf->SetMargins(8,8,8);
    $ans_pdf->SetAutoPageBreak(true,12);
    $worksheet = mt_rand(11111,99999);
    $worksheet = date('mdY') . $worksheet;
    $probCnt = $_POST['count'] * 1;
    if($probCnt > 100)$probCnt = 100;
    $def_val['count'] = $probCnt;
    
    if(is_array($_POST['opperands'])){
        $operandCnt = count($_POST['opperands']);
        $operations =  $_POST['opperands'];
    }else{
        $operandCnt = 4;
        $operations = array('addition',
                            'subtraction',
                            'multiplication',
                            'division');
    }
    $operandCnt--;
    foreach($operations as $opperation){
        $def_val[$opperation] = 'checked';
    }
    
    switch($_POST["algebraStyle"]){
        case '0':  //elementary Math
            $var = "?";
            $algebraStyle[3] = 1;
            $def_val['none'] = 'checked';
            break;
        case '1':  //Answer the question mark
            $var = "?";
            $algebraStyle = array(0=>1,
                                  1=>1,
                                  2=>1,
                                  3=>1
                                  );
            $def_val['qst'] = 'checked';
            break;
        case '2':  // Solve for X
            $var = "X";
            $algebraStyle = array(0=>1,
                                  2=>1,
                                  3=>1
                                  );
            $def_val['useX'] = 'checked';
            break;        
    }
    
    $places = $_POST['maxPlace'] * 1;
    $place_id = ($places + 1) / 10;
    $place_id = 'max' . $place_id;
    $def_val[$place_id] = 'checked';
    $problems = array();
    for($n=0;$n<$probCnt;$n++){
        $problem = array();
        $test = mt_rand(0,$operandCnt);
        switch($operations[$test]){
            case 'addition':
                $val = addition($places);
                $problem[0] = $val[0];
                $problem[1] = '+';
                $problem[2] = $val[1];
                $problem[3] = $val[2];
                $problem[4] = '+';
                break;
            case 'subtraction':
                $val = addition($places);
                if($val[0] > $val[2]){
                    $problem[0] = $val[0];
                    $problem[1] = '-';
                    $problem[2] = $val[2];
                    $problem[3] = $val[1];
                    $problem[4] = '-';
                }else{
                    $problem[0] = $val[2];
                    $problem[1] = '-';
                    $problem[2] = $val[0];
                    $problem[3] = $val[1];
                    $problem[4] = '-';
                }
                break;
            case 'multiplication':
                $val = multiplication($places);
                $problem[0] = $val[0];
                $problem[1] = 'x';
                $problem[2] = $val[1];
                $problem[3] = $val[2];
                $problem[4] = 'x';
                break;
            case 'division':
                $val = multiplication($places);
                $problem[0] = $val[2];
                $problem[1] = '&#247;';
                $problem[2] = $val[1];
                $problem[3] = $val[0];
                $problem[4] = chr(247);
                break;
        }
        $problems[] = $problem;
    }

    $prob_html = '<div class="paper"><table>';
    
    $first = 1;
    $pageY = 15;
    $pageX = 8;
    $prob_pdf->AddPage('P','Letter');
    $ans_pdf->AddPage('P','Letter');
    
    $string = 'Replace the ';
    $prob_pdf->SetFont('helvetica','B',10);
    $prob_pdf->setXY($pageX, $pageY);
    $prob_pdf->Write(0,$string,'');
    $prob_pdf->SetFont('helvetica','UB',12);
    $prob_pdf->Write(0,' '.$var.' ','');
    $prob_pdf->SetFont('helvetica','B',10);
    $string = ' with the value that makes each Math Fact true, there will be no decimals or remainders'; 
    $prob_pdf->Write(0,$string,'');
    $prob_pdf->SetFont('helvetica','',9);
    
    $pageX = 45;
    $string = '                                          Answer Key                                          ';
    $ans_pdf->SetFont('helvetica','UB',12);
    $ans_pdf->setXY($pageX, $pageY);
    $ans_pdf->Write(0,$string,'');
    $ans_pdf->SetFont('helvetica','',9);
    
    $pageY = 27;
    $pageX = 8;
    foreach($problems as $cnt => $problem){
        if($first == 1){
            $prob_html .= '<tr class="row" style="height:50px;">';
            
        }elseif($cnt % 4 == 0){
            $prob_html .= '</tr><tr style="height:50px;">';
            $pageY += 9;
            $pageX = 8;

        }
        $blank = mt_rand(0,3);
        while(!$algebraStyle[$blank]){
            $blank = mt_rand(0,3);    
        }
        
        $num = $cnt + 1;
        $prob_html .= '<td class="cell" style="border-bottom:solid;"><b>'.$num.'. </b> &nbsp;';
        
        $string = $num . '.  ';
        $prob_pdf->SetFont('helvetica','B',12);
        $prob_pdf->setXY($pageX, $pageY);
        $prob_pdf->Write(0,$string,'');
        $prob_pdf->SetFont('helvetica','',9);
        
        $ans_pdf->SetFont('helvetica','B',12);
        $ans_pdf->setXY($pageX, $pageY);
        $ans_pdf->Write(0,$string,'');
        $ans_pdf->SetFont('helvetica','',9);
        
        
        $space = '';
        for($i=0;$i<4;$i++){
            $prob_html .= $space;
            $prob_pdf->Write(0,' ','');
            
            $ans_pdf->Write(0,' ','');
            
            if($i == 3){
                $prob_html .= '=' . $space; 
                $prob_pdf->Write(0,' = ','');
                
                $ans_pdf->Write(0,' = ','');
            }
            if($i == $blank){
                $prob_html .= '<u> &nbsp; '.$var.' &nbsp;  </u>';
                $prob_pdf->SetFont('helvetica','UB',9);
                $prob_pdf->Write(0,' '.$var.' ','');
                $prob_pdf->SetFont('helvetica','',9);
                
                $ans_pdf->SetFont('helvetica','UB',9); 
                if($i == 1){
                    $ans_pdf->Write(0,$problem[4],'');
                }else{
                    $ans_pdf->Write(0,$problem[$i],'');
                }
                $ans_pdf->SetFont('helvetica','',9);
                
            }else{
                $prob_html .= $problem[$i];
                if($i == 1){
                    $prob_pdf->Write(0,$problem[4],'');
                    
                    $ans_pdf->Write(0,$problem[4],'');
                }else{
                    $prob_pdf->Write(0,$problem[$i],'');
                    
                    $ans_pdf->Write(0,$problem[$i],'');
                }
                
            }
            $space = ' &nbsp; ';
        }
        $prob_html .= '</td>';
        $pageX += 50;
        $prob_pdf->setXY($pageY, $pageX); 
        $ans_pdf->setXY($pageY, $pageX); 
        $first = 0; 
    }
    $pageY += 12;
    $pageX = 160;
    $prob_pdf->setXY($pageX, $pageY);
    $prob_pdf->SetFont('helvetica','',8);
    $prob_pdf->Write(0,$worksheet,'');
    
    $ans_pdf->setXY($pageX, $pageY);
    $ans_pdf->SetFont('helvetica','',8);
    $ans_pdf->Write(0,$worksheet,'');
    
    $prob_html .= '</tr></table>' . $worksheet . '</div>';
    
    $answerSheet = $ans_pdf->Output('S');
    $problemSheet = $prob_pdf->Output('S');
    
    $root = pathinfo($_SERVER['SCRIPT_FILENAME']); 

    $ans_name= 'sheets/key'.$worksheet.'.pdf';
    $prob_name= 'sheets/prob'.$worksheet.'.pdf';
    $ans_path = $root['dirname'] .'/'.$ans_name;
    $prob_path = $root['dirname'] .'/'.$prob_name;
    
    $h = fopen($ans_path,'w');
    fwrite($h,$answerSheet);
    fclose($h);
    
    $h = fopen($prob_path,'w');
    fwrite($h,$problemSheet);
    fclose($h);
    
    $links = '<div class="links" id="links"><span class="label" id="ansLink"><a href="'.$ans_name.'" download>Download the Answer Key</a></span><br><span class="label" id="probLink"><a href="'.$prob_name.'" download>Donwload the Problem Sheet</a></span></div>';
    
    
}

?>
<!DOCTYPE html>
<head>
<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<?php
    echo $links;
?>

<div class="heading"  id="title">
    <span class="label">To generate a math worksheet adjust the below settings and hit the submit button.<br></span>
    <span class="label">This page will then create a unique problem sheet and answer key.<br></span>
</div>
<form action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">     
<div class="count"  id="countBox">
    <label for="count" class="label">Number of Problems (max of 100):</label>
    <input type="text" class="css-input" name="count" id="count" value="<?php echo $def_val['count']; ?>"/>
</div>
<div class="criteria" id="placesBox">
    <span class="label">Max Places:</span><br>
    <input type="radio" class="css-checkbox" id="max1" name="maxPlace" value="9" <?php echo $def_val['max1']; ?>/><label for="max1" class="css-label-rdo">Ones</label> <br>
    <input type="radio" class="css-checkbox" id="max10" name="maxPlace" value="99" <?php echo $def_val['max10']; ?>/><label for="max10" class="css-label-rdo">Tens</label> <br>
    <input type="radio" class="css-checkbox" id="max100" name="maxPlace" value="999" <?php echo $def_val['max100']; ?>/><label for="max100" class="css-label-rdo">Hundreds</label> <br>
    <input type="radio" class="css-checkbox" id="max1000" name="maxPlace" value="9999" <?php echo $def_val['max1000']; ?>/><label for="max1000" class="css-label-rdo">Thousands</label> <br>
</div>
<div class="criteria" id="opperandsBox"><span class="label">Opperands:</span><br>
    <input type="checkbox" class="css-checkbox" id="addition" name="opperands[]" value="addition" <?php echo $def_val['addition']; ?>/><label class="css-label-chk" for="addition">Addition</label><br>
    <input type="checkbox" class="css-checkbox" id="subtraction" name="opperands[]" value="subtraction" <?php echo $def_val['subtraction']; ?>/><label class="css-label-chk" for="subtraction">Subtraction</label><br>
    <input type="checkbox" class="css-checkbox" id="multiplication" name="opperands[]" value="multiplication" <?php echo $def_val['multiplication']; ?>/><label class="css-label-chk" for="multiplication">Multiplication</label><br> 
    <input type="checkbox" class="css-checkbox" id="division" name="opperands[]" value="division" <?php echo $def_val['division']; ?>/><label class="css-label-chk" for="division">Division</label><br>
</div>
<div class="criteria" id="algebra">
    <span class="label">Problem style:</span><br>
    <input type="radio" class="css-checkbox" id="none" name="algebraStyle" value="0" <?php echo $def_val['none']; ?>/><label for="none" class="css-label-rdo">Blank always after =</label> <br>
    <input type="radio" class="css-checkbox" id="qst" name="algebraStyle" value="1" <?php echo $def_val['qst']; ?>/><label for="qst" class="css-label-rdo">Random blank</label> <br>
    <input type="radio" class="css-checkbox" id="useX" name="algebraStyle" value="2" <?php echo $def_val['useX']; ?>/><label for="useX" class="css-label-rdo">Solve for X</label> <br>
</div>
<div class="criteria" id="">
    <input type="hidden" name="secCntrl" value="<?php echo $val_ses; ?>" />
    <input type="submit" class="css-input"/>
</div>
</form>
<?php
    echo $prob_html;
?>
<footer>
<div class="footer">
    Covidiversity is free software offered under the GPL, a copy of which can be found <a href="license.txt">here</a><br>
    The repository for this software can be found <a href="https://bitbucket.org/JamesWithGoose/covidiversity/src/master/">here</a>
</div>
</footer>
</body>
