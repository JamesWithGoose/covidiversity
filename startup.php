<?php
/***********************************
* Author: James Holden
* Created: 3/25/2020
* Covidiversity is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
***********************************/

include_once('fpdf/fpdf.php');

define('CRLF','
');
$ts = date('h:i:s m/d/Y');
$log_str = '"'.$_SERVER['REMOTE_ADDR'].'","'.$ts.'","'.$_SERVER['HTTP_REFERER'].'","'.$_SERVER['UNIQUE_ID'].'","'.$_SERVER['HTTP_USER_AGENT'].'","'.$_SESSION['access_log']++.'"'.CRLF;
$h = fopen('log','a');
fwrite($h,$log_str);
fclose($h);

function prh($myvar){
    echo '<pre>'.str_replace(array("\n" , " "), array('<br>', '&nbsp;'), print_r($myvar, true)).'</pre>';
}
function is_valid_email_address($email){
    $qtext = '[^\\x0d\\x22\\x5c\\x80-\\xff]';
    $dtext = '[^\\x0d\\x5b-\\x5d\\x80-\\xff]';
    $atom = '[^\\x00-\\x20\\x22\\x28\\x29\\x2c\\x2e\\x3a-\\x3c'.
    '\\x3e\\x40\\x5b-\\x5d\\x7f-\\xff]+';
    $quoted_pair = '\\x5c[\\x00-\\x7f]';
    $domain_literal = "\\x5b($dtext|$quoted_pair)*\\x5d";
    $quoted_string = "\\x22($qtext|$quoted_pair)*\\x22";
    $domain_ref = $atom;
    $sub_domain = "($domain_ref|$domain_literal)";
    $word = "($atom|$quoted_string)";
    $domain = "$sub_domain(\\x2e$sub_domain)*";
    $local_part = "$word(\\x2e$word)*";
    $addr_spec = "$local_part\\x40$domain";
    return preg_match("!^$addr_spec$!", $email) ? 1 : 0;
}
function guid(){
    if (function_exists('com_create_guid')){
        return com_create_guid();
    }else{
        $key_val = array(8,9,'A','B');
        $charid = strtoupper(bin2hex(gen_rand()));
        $hyphen = chr(45);// "-"
        $uuid = chr(123)// "{"
        .substr($charid, 0, 8).$hyphen
        .substr($charid, 8, 4).$hyphen
        .'4'
        .substr($charid,13, 3).$hyphen
        .$key_val[rand(0,3)]
        .substr($charid,17, 3).$hyphen
        .substr($charid,20,12)
        .chr(125);// "}"
        return $uuid;
    }
}
function gen_rand($len=256){
    $h = fopen('/dev/urandom','r');
    $rand = fread($h,$len);
    fclose($h);
    return $rand;
}
function addition($places){
    $val = array();
    $maxVal = mt_rand(0,$places);
    $val[0] = mt_rand(0,$maxVal);
    $max = $maxVal - $val[0];
    $val[1] = mt_rand(0,$max);
    $val[2] = $val[0] + $val[1];
    $key = $val[0].$val[1].$val[2];
    while($_SESSION['maxDupe'] < $_SESSION['no_dupes'][$key]
          or $_SESSION['skip_vals'][$val[0]]
          or $_SESSION['skip_vals'][$val[1]]
          or $_SESSION['skip_vals'][$val[2]]
          ){
        $maxVal = mt_rand(0,$places);
        $val[0] = mt_rand(0,$maxVal);
        $max = $maxVal - $val[0];
        $val[1] = mt_rand(0,$max);
        $val[2] = $val[0] + $val[1];
        $key = $val[0].$val[1].$val[2];
    }
    
    
    $key = $val[0].$val[1].$val[2];
    $_SESSION['no_dupes'][$key]++;
    if($_SESSION['maxDupe'] < $_SESSION['no_dupes'][$key]){
        $_SESSION['maxDupe'] = $_SESSION['no_dupes'][$key];    
    }
    $_SESSION['skip_vals'] = array($val[0] => 1,
                                   $val[1] => 1,
                                   $val[2] => 1);
    return $val;
}
function multiplication($places){
    $val = array();
    $maxVal = mt_rand(0,$places);
    $val[0] = mt_rand(0,$maxVal);
    $val[1] = mt_rand(0,$maxVal);
    $val[2] = $val[0] * $val[1];
    $key = $val[0].$val[1].$val[2];
    while($val[2] > $places 
          or $val[2] == 0 
          or $_SESSION['maxDupe'] < $_SESSION['no_dupes'][$key]
          or $_SESSION['skip_vals'][$val[0]]
          or $_SESSION['skip_vals'][$val[1]]
          or $_SESSION['skip_vals'][$val[2]]
          ){
        $maxVal = mt_rand(0,$places);
        $val[0] = mt_rand(0,$maxVal);
        $val[1] = mt_rand(0,$maxVal);
        $val[2] = $val[0] * $val[1];
        $key = $val[0].$val[1].$val[2];    
    }
    $key = $val[0].$val[1].$val[2];
    $_SESSION['no_dupes'][$key]++;
    if($_SESSION['maxDupe'] < $_SESSION['no_dupes'][$key]){
        $_SESSION['maxDupe'] = $_SESSION['no_dupes'][$key];    
    }
    $_SESSION['skip_vals'] = array($val[0] => 1,
                                   $val[1] => 1,
                                   $val[2] => 1);
    return $val;
}
function emailer($send_to,$htmlbody,$subject){
        
        $return_email = "covidiversityy@ansersys.com";

$html_email =<<<MSG
        <!DOCTYPE html>
        <html>
        <head>
        <META http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <TITLE>Goose IAS</TITLE>
        </head>
        <body>
        <div id="main">
        $htmlbody
        </div>
        <br><br>
        <div id="footer">
        </div>
        </body>
        </html>
MSG;
    
        $crlf = "\r\n";
        $email_date = date("D, j M Y H:i:s O");

        $headers = "From: $return_email" .$crlf;
        $headers .= "Reply-To: $return_email" .$crlf;
        $headers .= "Return-Path: $return_email" .$crlf;
        $headers .= "X-Mailer: $return_email" .$crlf;
        $headers .= "Date: $email_date " .$crlf;
        $headers .= "X-Sender-IP: $_SERVER[REMOTE_ADDR]" .$crlf;
        
        /* additional content type headers*/
        $headers  .= "MIME-Version: 1.0" .$crlf;

        $boundary = '_'.md5(uniqid(time()));

        $headers .= "Content-Type: multipart/mixed; charset = \"iso-8859-1\";" .$crlf;
        $headers .= "\tboundary=\"" .$boundary ."\";" .$crlf .$crlf;

        $message = "If you can see this MIME than your client doesn't accept MIME types!" .$crlf;
        $message .= "--". $boundary .$crlf;
        $message .= "Content-Type: text/html; charset = \"iso-8859-1\";" .$crlf;
        $message .= "Content-Transfer-Encoding: 7bit" .$crlf .$crlf;
        $message .= $html_email .$crlf .$crlf;

        $message .= $crlf ."--" .$boundary ."--" .$crlf .$crlf;
        mail($send_to,$subject, $message, $headers);
        
    }
?>
